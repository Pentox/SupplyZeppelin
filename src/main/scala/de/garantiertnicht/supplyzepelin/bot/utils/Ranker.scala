/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.utils

import de.garantiertnicht.supplyzepelin.persistence.entities.ScoreboardEntry

case class RankedMember(entry: ScoreboardEntry, score: Long, place: Long) {
  lazy val member = entry._id.getAsMember()
}

object Ranker {
  def rank(members: Seq[ScoreboardEntry], page: Int = 0, perPage: Int = Int.MaxValue): Seq[RankedMember] = {
    var place = 0L
    var lastScore = Long.MaxValue

    members
      .filter(_.blocked.isEmpty)
      .sortBy(-_.score.total)
      .slice(0, (page + 1) * perPage)
      .map(info ⇒ {
        val score = info.score.total

        if(score < lastScore) {
          lastScore = score
          place += 1
        }

        RankedMember(info, info.score.total, place)
      })
      .slice(page * perPage, (page + 1) * perPage)
  }
}
