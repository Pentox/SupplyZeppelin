/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import java.util.concurrent.TimeUnit

import akka.actor.Actor
import com.mongodb.client.model.UpdateOptions
import de.garantiertnicht.supplyzepelin.bot.utils.Ranker
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.definitions.{GetRewards, GetScoreboard}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{Reward, RewardData}
import de.garantiertnicht.supplyzepelin.persistence.entities.{RewardInfo, ScoreboardEntry}
import net.dv8tion.jda.core.entities.{Guild, Member}
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Updates.set

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

case class ApplyRewards(guild: Guild, members: Seq[Member], rewards: Seq[Reward], invocation: Int = 0)
case class Recalculate(guild: Guild, additionalRewards: Seq[Reward] = Seq())

class RewardActor extends Actor {
  implicit val executionContext: ExecutionContext = context.system.dispatcher

  override def receive: PartialFunction[Any, Unit] = {
    case ApplyRewards(guild, members, rewards, invocation) ⇒ calculate(guild, members, rewards, invocation)
    case Recalculate(guild, additional) ⇒
      new GetRewards(guild.getIdLong).mongoQuery.toFuture().onComplete {
        case Success(rewards) ⇒ calculate(guild, guild.getMembers.asScala, (additional ++: rewards).distinct)
        case Failure(e) =>
          e.printStackTrace()
      }
  }

  def calculate(guild: Guild, members: Seq[Member], rewards: Seq[Reward], invocation: Int = 0): Unit = {
    if(members.isEmpty || rewards.isEmpty) {
      return
    }

    val now = System.currentTimeMillis()

    if(invocation >= 10) {
      RewardActor.timeouts.put(guild.getIdLong, now + 10000)
      println(s"Runaway rewards for ${guild.getName} (${guild.getId})")
      return
    } else if(invocation == 0) {
      RewardActor.timeouts.get(guild.getIdLong).foreach({ time ⇒
        if(time > now) {
          return
        }
      })
    }

    RewardActor.timeouts.put(guild.getIdLong, now + 1000)

    println(invocation + " " + rewards.map(_._id.name).mkString(", "))

    val orderedRewards = rewards.sortBy(_.options.priority)

    var recalculate = false

    val informationAuthoritative = Await.result(new GetScoreboard(guild).mongoQuery.toFuture(), Duration(1, TimeUnit.MINUTES)).filter(info => members.map(_.getUser.getIdLong).contains(info._id.userId))
    val information: Seq[ScoreboardEntry] = members.filter(member ⇒ !informationAuthoritative.map(_._id.userId).contains(member.getUser.getIdLong))
      .map(ScoreboardEntry.default(_)) ++: informationAuthoritative

    val emptyRewardData = RewardData(Seq(), Seq())
    val upsert = new UpdateOptions().upsert(true)

    Ranker.rank(information).foreach(rankedMember ⇒ {

      val oldRewards = orderedRewards.filter(reward => rankedMember.entry.rewards.granted.contains(reward._id.name))
      val newRewards: Seq[Reward] = (orderedRewards.filter(_.requirementsMet(rankedMember)) ++: oldRewards.filter(reward ⇒ !reward.options.remove)).distinct.sortBy(_.options.priority)

      val newCache = if(newRewards.isEmpty) {
        Seq()
      } else {
        newRewards.map(_.data).reduceRight((a, b) ⇒ a.merge(b).squash.getOrElse(emptyRewardData)).grants
          .filter(_.cached)
      }

      if(oldRewards.nonEmpty || newRewards.nonEmpty) {
        rankedMember.member.foreach(member ⇒ {
          (oldRewards.map(_.reverse) ++: newRewards).sortBy(_.options.priority).map(_.data)
            .reduceLeft(_.merge(_).squash.foreach(reward ⇒ {
              reward.applyReward(member)
              Connection.scoreboard.updateOne(equal("_id", rankedMember.entry._id), set("rewards", RewardInfo(newRewards
                .map(_._id.name), newCache)), upsert).head()

              recalculate = true
            }))
        })
      }
    })

    if(recalculate) {
      self ! ApplyRewards(guild, members, orderedRewards, invocation + 1)
    }
  }
}

object RewardActor {
  val timeouts = mutable.Map[Long, Long]()
}
