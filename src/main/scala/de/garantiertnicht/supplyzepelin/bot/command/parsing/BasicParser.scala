/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */
package de.garantiertnicht.supplyzepelin.bot.command.parsing

import de.garantiertnicht.supplyzepelin.bot.permissions.commands._
import de.garantiertnicht.supplyzepelin.bot.permissions.other.{Collect, Custom}

import scala.util.parsing.combinator.JavaTokenParsers

abstract class BasicParser extends JavaTokenParsers {

  def string: Parser[String] = "[^\\s]+".r | stringLiteral | ("`"+"""([^"\x00-\x1F\x7F\\]|\\[\\`bfnrt]|\\u[a-fA-F0-9]{4})*"""+"`").r
  def braced[A](parser: Parser[A]): Parser[A] = ("(" ~> parser <~ ")") | ("[" ~> parser <~ "]") | ("{" ~> parser <~ "}") | ("<" ~> parser <~ ">") | parser

  def byteConst: Parser[Byte] = wholeNumber ^^ { _.toByte }
  def shortConst: Parser[Short] = wholeNumber ^^ { _.toShort }
  def intConst: Parser[Int] = wholeNumber ^^ { _.toInt }
  def longConst: Parser[Long] = wholeNumber ^^ { _.toLong }

  def permissionBribe = Bribe.name ^^ { _ ⇒ Bribe }
  def permissionCollect = Collect.name ^^ { _ ⇒ Collect }
  def permissionCheckPoints = CheckPoints.name ^^ { _ ⇒ CheckPoints }
  def permissionHelp = Help.name ^^ { _ ⇒ Help }
  def permissionManageRewards = ManageRewards.name ^^ { _ ⇒ ManageRewards }
  def permissionRedeem = Redeem.name ^^ { _ ⇒ Redeem }
  def permissionRedeemOthers = RedeemOthers.name ^^ { _ ⇒ RedeemOthers }
  def permissionShowPoints = ShowPoints.name ^^ { _ ⇒ ShowPoints }

  def custom1 = Custom.custom1.name ^^ { _ ⇒ Custom.custom1 }
  def custom2 = Custom.custom2.name ^^ { _ ⇒ Custom.custom2 }
  def custom3 = Custom.custom3.name ^^ { _ ⇒ Custom.custom3 }
  def custom4 = Custom.custom4.name ^^ { _ ⇒ Custom.custom4 }
  def custom5 = Custom.custom5.name ^^ { _ ⇒ Custom.custom5 }
  def custom6 = Custom.custom6.name ^^ { _ ⇒ Custom.custom6 }
  def custom7 = Custom.custom7.name ^^ { _ ⇒ Custom.custom7 }
  def custom8 = Custom.custom8.name ^^ { _ ⇒ Custom.custom8 }
  def custom9 = Custom.custom9.name ^^ { _ ⇒ Custom.custom9 }
  def custom10 = Custom.custom10.name ^^ { _ ⇒ Custom.custom10 }
  def custom11 = Custom.custom11.name ^^ { _ ⇒ Custom.custom11 }
  def custom12 = Custom.custom12.name ^^ { _ ⇒ Custom.custom12 }
  def custom13 = Custom.custom13.name ^^ { _ ⇒ Custom.custom13 }
  def custom14 = Custom.custom14.name ^^ { _ ⇒ Custom.custom14 }
  def custom15 = Custom.custom15.name ^^ { _ ⇒ Custom.custom15 }
  def custom16 = Custom.custom16.name ^^ { _ ⇒ Custom.custom16 }
  def custom17 = Custom.custom17.name ^^ { _ ⇒ Custom.custom17 }
  def custom18 = Custom.custom18.name ^^ { _ ⇒ Custom.custom18 }
  def custom19 = Custom.custom19.name ^^ { _ ⇒ Custom.custom19 }
  def custom20 = Custom.custom20.name ^^ { _ ⇒ Custom.custom20 }
  def custom21 = Custom.custom21.name ^^ { _ ⇒ Custom.custom21 }
  def custom22 = Custom.custom22.name ^^ { _ ⇒ Custom.custom22 }
  def custom23 = Custom.custom23.name ^^ { _ ⇒ Custom.custom23 }
  def custom24 = Custom.custom24.name ^^ { _ ⇒ Custom.custom24 }

  def permissionsNormal = permissionBribe | permissionCollect | permissionCheckPoints | permissionHelp |
    permissionManageRewards | permissionRedeem | permissionRedeemOthers | permissionShowPoints | failure("Non-Custom Permission expected")
  def permissionCustom = custom1 | custom2 | custom3 | custom4 | custom5 | custom6 | custom7 | custom8 | custom9 | custom10 | custom11 | custom12 | custom13 | custom14 | custom15 | custom16 | custom17 | custom18 | custom19 | custom20 | custom21 | custom22 | custom23 | custom24 | failure("Custom Permission Expected")
  def permission = permissionsNormal | permissionCustom | failure("Permission expected")
}
