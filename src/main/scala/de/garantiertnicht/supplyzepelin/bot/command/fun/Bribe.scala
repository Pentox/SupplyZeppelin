/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.fun

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.Bribe
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.definitions.{GetInfoForMember, UpdateScore}
import de.garantiertnicht.supplyzepelin.persistence.entities.{MemberInfo, Score, ScoreboardEntry}
import net.dv8tion.jda.core.entities.Message

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object Bribe extends Command {
  override val name: String = "bribe"
  implicit val timeout = Timeout(1 minute)

  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    if(commandAndArgs.size > 1) {
      val amount = try {
        commandAndArgs(1).toLong
      } catch {
        case _: NumberFormatException => 0
      }

      if(amount < 10) {
        originalMessage.getChannel.sendMessage("You must specify a bribe amount greater or equal than 10!").queue()
      } else {
        new GetInfoForMember(originalMessage.getMember).mongoQuery.head().onSuccess {
          case info: ScoreboardEntry =>
            val score = info.score.total
            if(score < amount) {
              originalMessage.getChannel.sendMessage(s"You only have $score points, ${originalMessage.getMember.getAsMention}!").queue()
            } else {
              guild ! new Bribe(originalMessage.getMember, amount)
              UpdateScore(originalMessage.getMember, Score(-amount)).mongoQuery.head()
              originalMessage.getChannel.sendMessage(s"Added a bribe for ${originalMessage.getMember.getAsMention} with $amount points. You will get +${amount * 2} points if you open the next crate, or else the winner gets ${amount / 3} points extra.").queue()
            }
        }
      }
    }
  }
}
