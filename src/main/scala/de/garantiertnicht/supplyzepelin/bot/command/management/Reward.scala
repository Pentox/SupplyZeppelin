/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.command.parsing.RewardParser
import de.garantiertnicht.supplyzepelin.persistence.definitions.{AddReward, RewardExists}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.Reward
import net.dv8tion.jda.core.entities.Message

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Reward extends Command {
  override val name = "reward"
  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex:
  ExecutionContext): Unit = {
    if(commandAndArgs.length < 3) {
      originalMessage.getChannel.sendMessage(s"${originalMessage.getMember.getAsMention} Usage: reward <create> <reward definition>").queue()
      return
    }

    val definition = commandAndArgs.slice(2, commandAndArgs.length).mkString(" ").toUpperCase.replace('\n', ' ')
    commandAndArgs(1).toLowerCase match {
      case "create" ⇒
        val parser = new RewardParser(originalMessage.getGuild)
        parser.parse(parser.oneReward, definition + " END") match {
          case parser.Success(fullReward: Reward, _) ⇒
            fullReward.squash match {
              case Some(reward) ⇒
                new RewardExists(reward._id).mongoQuery.head().onComplete {
                  case Success(false) =>
                      new AddReward(reward).mongoQuery.head().onComplete({
                        case Success(_) ⇒
                          originalMessage.getChannel
                          .sendMessage(s"${originalMessage.getMember.getAsMention} added a reward with the definition `$reward`!")
                          .queue()
                        case Failure(e) => e.printStackTrace()
                      })
                  case Success(true) =>
                      originalMessage.getChannel.sendMessage(s"There is already a reward with that name, ${originalMessage.getMember.getAsMention}!").queue()
                  case fail: Failure[Boolean] ⇒
                    originalMessage.getChannel.sendMessage(s"The database returned a failure, please try again later ${originalMessage.getMember.getAsMention}.").queue()
                    fail.exception.printStackTrace()
                }
              case None ⇒
                originalMessage.getChannel.sendMessage(s"You may not create an empty reward, ${originalMessage.getMember.getAsMention}!").queue()
            }
          case parser.NoSuccess(message, _) ⇒
            originalMessage.getChannel.sendMessage(s"${originalMessage.getMember.getAsMention} The reward definition could not be parsed: `${message.replace('`', '\'')}`").queue()
        }
      case option ⇒
        originalMessage.getChannel
          .sendMessage(s"${originalMessage.getMember.getAsMention} Unknown operation $option.").queue()
    }
  }
}
