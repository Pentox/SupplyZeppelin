/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.parsing

import de.garantiertnicht.supplyzepelin.persistence.entities.rewards._
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{Roles, ScoreAmount}
import net.dv8tion.jda.core.entities.Guild

class RewardParser(guild: Guild) extends GuildParser(guild) {
  def score: Parser[ScoreAmount] = braced(opt(longConst) ~ opt(":" ~> longConst)) <~ "POINTS" ^^ {case (score ~ static) ⇒ ScoreAmount(score.getOrElse(0), static.getOrElse(0))}

  def roles: Parser[Roles] = "ROLES" ~> opt(braced("APPROVED" ~> braced(rep1sep(role, ",")))) ~ opt(braced("DENIED" ~> braced(rep1sep(role, ",")))) ^^ {case (approved ~ denied) ⇒
    Roles(
      approved.getOrElse(Seq()).filter(_.isDefined).map(_.get.getIdLong),
      denied.getOrElse(Seq()).filter(_.isDefined).map(_.get.getIdLong)
    )
  }

  def grant: Parser[RewardEntity] = braced(score | roles)
  def requirement: Parser[RewardEntity] = braced(score | roles)

  def rewardData: Parser[RewardData] = braced("REQUIRES" ~> braced(rep1sep(requirement, ",")) ~ braced("GRANTS" ~> braced(rep1sep(grant, ",")))) ^^ { case(requirement ~ grant) ⇒
    RewardData(requirement, grant)
  }

  def optionPriority(options: RewardOptions) = "PRIORITY" ~> intConst ^^ { value ⇒ options.copy(optPriority = Some(value)) }
  def optionPermanent(options: RewardOptions) = "PERMANENT" ^^ { (_) ⇒ options.copy(optRemove = Some(false)) }
  def optionNonPermanent(options: RewardOptions) = "LOSABLE" ^^ { (_) ⇒ options.copy(optRemove = Some(true)) }

  def option(options: RewardOptions) = braced(optionPriority(options) | optionPermanent(options) | optionNonPermanent(options))

  def reward(optionsInstance: RewardOptions = RewardOptions.default): Parser[Reward] = string ~ rewardData ~ opt(braced("SET" ~> braced(rep1sep(option(optionsInstance), ","))))  ^^ {
    case (name ~ data ~ options) ⇒
      val optionsToUse: Option[RewardOptions] = if(options.isDefined) {
        val effective = options.get.reduce { (o, n) ⇒
          if(n.optRemove.nonEmpty) o.copy(optRemove = n.optRemove)
          else if(n.optPriority.nonEmpty) o.copy(optPriority = n.optPriority)
          else o
        }

        if(effective != optionsInstance) {
          Some(effective)
        } else {
          None
        }
      } else {
        None
      }

      Reward(RewardIdentifier(guild.getIdLong, name), data, optionsToUse)
  }

  def oneReward = reward() <~ "END"
}
