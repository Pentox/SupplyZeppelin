/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.fun

import akka.actor.ActorRef
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.Transaction
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.definitions.GetInfoForMember
import de.garantiertnicht.supplyzepelin.persistence.entities.ScoreboardEntry
import net.dv8tion.jda.core.entities.Message

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

object Transfer extends Command {
  override val name = "transfer"
  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex:
  ExecutionContext): Unit = {
    if(commandAndArgs.length < 3) {
      originalMessage.getChannel.sendMessage(s"You must specify an amount to transfer and at least one user, ${originalMessage.getMember.getAsMention}!.").queue()
    } else {
      try {
        val amount = commandAndArgs(1).toInt
        if(amount < 20) {
          originalMessage.getChannel.sendMessage(s"You must at least transfer 20 points, ${originalMessage.getMember.getAsMention}!").queue()
          return
        } else if(amount > Main.MAX_POINT_MODIFICATION) {
          originalMessage.getChannel.sendMessage(Main.maxPointsModificationExceededMessage(originalMessage.getMember)).queue()
          return
        }

        val users = if(originalMessage.mentionsEveryone || originalMessage.getRawContent.contains("@everyone") || originalMessage.getRawContent.contains("@here")) {
          originalMessage.getGuild.getMembers.asScala
        } else {
          originalMessage.getMentionedUsers.asScala.map(originalMessage.getGuild.getMember(_)).filter(_ != null)
        }

        if(users.isEmpty) {
          originalMessage.getChannel.sendMessage(s"📎 It looks like ${originalMessage.getAuthor.getAsMention} has sent me creative mentions. Would he/she/it/space alien like help?").queue()
          return
        }

        val totalAmount = amount * users.length
        implicit val timeout = Timeout(1 minute)

        new GetInfoForMember(originalMessage.getMember).mongoQuery.head().onSuccess{
          case entry: ScoreboardEntry =>
            val score = entry.score.total
            if(score >= totalAmount) {
              val tax = 10 + amount / 6
              val amountPerUser = amount - tax
              val totalTax = tax * users.length

              guild ! Transaction(-totalAmount, Seq(originalMessage.getMember), false)
              guild ! Transaction(amountPerUser, users)

              val notifyMentions = if(users.length > 10) {
                users.filter(_.getUser.isBot).map(_.getAsMention).mkString(", ") + " and all other users above"
              } else {
                users.map(_.getAsMention).mkString(", ")
              }

              originalMessage.getChannel.sendMessage(s"$notifyMentions has/have each received $amountPerUser points from ${originalMessage.getMember.getAsMention}! Here is your receipt: ```diff\n" +
                s"+$totalAmount Points\n-$totalTax Points (Transaction Fee)\n=${totalAmount - totalTax} Points```").queue()
            } else {
              originalMessage.getChannel.sendMessage(s"You have insufficient funds, ${originalMessage.getAuthor.getAsMention}!").queue()
              return
            }
        }
      } catch {
        case _: NumberFormatException ⇒ originalMessage.getChannel.sendMessage(s"The amount must be numeric, ${originalMessage.getAuthor.getAsMention}!").queue()
      }
    }
  }
}
