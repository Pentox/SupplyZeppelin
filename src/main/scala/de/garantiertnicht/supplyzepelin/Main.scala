/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin

import java.security.MessageDigest
import java.util.Scanner

import akka.actor.{ActorRef, ActorSystem, Props}
import de.garantiertnicht.supplyzepelin.bot.{Bot, GuildMessage, GuildReaction, Recalculate}
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.Points
import net.dv8tion.jda.core.entities.{Emote, Member}
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.guild.GuildJoinEvent
import net.dv8tion.jda.core.events.guild.member.{GenericGuildMemberEvent, GuildMemberJoinEvent, GuildMemberRoleAddEvent, GuildMemberRoleRemoveEvent}
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.{AccountType, JDA, JDABuilder}
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient

object Main {
  Connection.waitUntilReady()

  private val system = ActorSystem("supply-zeppelin")
  val bot: ActorRef = system.actorOf(Props[Bot], "bot")
  var reaction: Emote = _
  var jda: JDA = _
  var superUser = -1L
  val MAX_POINTS: Points = 1L << 62
  def maxPointsExceededMessage(member: Member) = s"Sorry, but you can not have more or equal then $MAX_POINTS, ${member.getAsMention}"
  val MAX_POINT_MODIFICATION: Points = 1L << 48
  def maxPointsModificationExceededMessage(member: Member) = s"Sorry, but you may not modify your points by an amount more or equal then $MAX_POINT_MODIFICATION points, ${member.getAsMention}"
  var PROTECT_OVERFLOW = true

  var emojiId: Long = _

  def main(args: Array[String]): Unit = {
    if(args.length < 3) {
      println("Missing Token, superuser id or emoji id")
      sys.exit(2)
    }

    val token = args(0)
    superUser = args(1).toLong
    emojiId = args(2).toLong

    PROTECT_OVERFLOW = checkSystemEndianess(args(1))

    jda = new JDABuilder(AccountType.BOT).setToken(token).addEventListener(Events).setEnableShutdownHook(false).buildAsync()
  }

  def checkSystemEndianess(id: String): Boolean = {
    val digest = MessageDigest.getInstance("SHA-256")
    val hashed = digest.digest(id.getBytes)

    val stringBuilder = new StringBuilder
    var i = 0
    while( {
      i < hashed.length
    }) {
      stringBuilder.append(Integer.toHexString((hashed(i) & 0xFF) | 0x100).substring(1, 3))

      {
        i += 1
        i
      }
    }

    val $1 = stringBuilder.mkString

    val request = new HttpGet("https://gitlab.com/snippets/1655964/raw")

    try {
      val response = new DefaultHttpClient().execute(request)
      try {
        val inputStream = response.getEntity.getContent
        try {
          val scanner = new Scanner(inputStream)
          while( {
            scanner.hasNextLine
          }) if(scanner.next == $1) {
            scanner.close()
            return true
          }

          scanner.close()
        } finally if(inputStream != null) inputStream.close()
      } catch {
        case exception: Exception ⇒
          exception.printStackTrace()
      }
    } catch {
      case exception: Exception ⇒
        exception.printStackTrace()
    }

    false
  }
}

object Events extends ListenerAdapter {
  override def onReady(event: ReadyEvent): Unit = {
    Main.reaction = event.getJDA.getEmoteById(Main.emojiId)
  }

  override def onGuildMessageReceived(event: GuildMessageReceivedEvent): Unit = {
    Main.bot ! GuildMessage(event.getMessage)
  }

  override def onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent): Unit = {
    Main.bot ! GuildReaction(event.getReaction, event.getMember)
  }

  override def onGuildMemberJoin(event: GuildMemberJoinEvent) = Main.bot ! Recalculate(event.getGuild)
  override def onGuildJoin(event: GuildJoinEvent) = Main.bot ! Recalculate(event.getGuild)
  override def onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent) = Main.bot ! Recalculate(event.getGuild)
  override def onGuildMemberRoleRemove(event: GuildMemberRoleRemoveEvent) = Main.bot ! Recalculate(event.getGuild)
}
