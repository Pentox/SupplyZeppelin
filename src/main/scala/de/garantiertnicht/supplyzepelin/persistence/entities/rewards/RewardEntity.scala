/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.entities.rewards

import de.garantiertnicht.supplyzepelin.bot.utils.RankedMember
import de.garantiertnicht.supplyzepelin.persistence.definitions.UpdateScore
import de.garantiertnicht.supplyzepelin.persistence.entities
import de.garantiertnicht.supplyzepelin.persistence.entities.{Points, RoleId, roleFromId}
import net.dv8tion.jda.core.entities.{Member, Role}
import scala.collection.JavaConverters._

sealed class RewardEntity {
  def merge(rewardEntity: RewardEntity): RewardEntity = this
  def squash: Option[RewardEntity] = None

  def isReward: Boolean = false
  def applyReward(member: Member): Unit = {}
  def reverse: RewardEntity = new RewardEntity
  def cached: Boolean = false

  def isRequirement: Boolean = false
  def requirementsMet(member: RankedMember): Boolean = false

  def createSingleUpdate = false
}

case class Roles(approves: Option[Seq[RoleId]], denies: Option[Seq[RoleId]]) extends RewardEntity {
  override def isReward = true

  override def applyReward(member: Member) =
    member.getGuild.getController
      .modifyMemberRoles(member, map(approves.getOrElse(Seq())).asJavaCollection, map(denies.getOrElse(Seq()))
        .asJavaCollection).queue()

  override def cached = false

  override def reverse = {
    Roles(None, approves)
  }

  override def merge(rewardEntity: RewardEntity): RewardEntity = rewardEntity match {
    case other: Roles ⇒
      val approvesNew = approves.getOrElse(Seq()).filterNot(other.denies.getOrElse(Seq()).contains(_)) ++ other.approves.getOrElse(Seq())
      val deniesNew = denies.getOrElse(Seq()).filterNot(other.approves.getOrElse(Seq()).contains(_)) ++ other.denies.getOrElse(Seq())
      Roles(approvesNew, deniesNew)
  }

  override def squash = {
    val add = approves.filter(!denies.contains(_))
    val remove = denies.filter(!approves.contains(_))

    if(add.nonEmpty || remove.nonEmpty) {
      Some(Roles(add, remove))
    } else {
      None
    }
  }

  override def isRequirement = true

  override def requirementsMet(member: RankedMember) = {
    member.entry._id.getAsMember() match {
      case Some(member: Member) ⇒
        member.getRoles.containsAll(map(approves.getOrElse(Seq())).asJavaCollection) &&
          !member.getRoles.asScala.exists(map(denies.getOrElse(Seq())).contains(_))
      case None ⇒
        false
    }
  }

  def map(roles: Seq[RoleId]): Seq[Role] = roles.map(id ⇒ roleFromId(id)).filter(_.isDefined).map(_.get)

  override def toString = {
    val approvesPart = if(approves.nonEmpty) {
      s" APPROVED (${
        approves.getOrElse(Seq()).map(roleFromId(_).map(_.getName).getOrElse("deleted-role")).mkString(", ")
      })"
    } else {
      ""
    }

    val deniesPart = if(denies.nonEmpty) {
      s" DENIED (${denies.getOrElse(Seq()).map(roleFromId(_).map(_.getName).getOrElse("deleted-role")).mkString(", ")})"
    } else {
      ""
    }

    s"ROLES$approvesPart$deniesPart"
  }
  override def createSingleUpdate = true
}

object Roles {
  def apply(approves: Seq[RoleId], denies: Seq[RoleId]): Roles = {
    val approvesOption = if(approves.isEmpty) {
      None
    } else {
      Some(approves)
    }

    val deniesOption = if(denies.isEmpty) {
      None
    } else {
      Some(denies)
    }

    Roles(approvesOption, deniesOption)
  }
}

case class ScoreAmount(score: Points, static: Points) extends RewardEntity {
  override def isReward = true

  override def applyReward(member: Member) = {
    UpdateScore(member, entities.Score(score, static)).mongoQuery.head()
  }

  override def cached = false

  override def reverse = {
    ScoreAmount(-score, -static)
  }

  override def merge(rewardEntity: RewardEntity) = {
    rewardEntity match {
      case other: ScoreAmount ⇒ ScoreAmount(score + other.score, static + other.static)
    }
  }

  override def squash = {
    if(score != 0 || static != 0) {
      Some(this)
    } else {
      None
    }
  }

  override def isRequirement = true

  override def requirementsMet(member: RankedMember): Boolean = {
    val scoreEntry: entities.Score = member.entry.score

    if(static == 0) {
      val scoreIn = scoreEntry.total

      if(score > 0) {
        score >= scoreIn
      }
      else {
        score <= scoreIn
      }
    } else {
      val scoreIsMatched = if(score != 0) {
        if(scoreEntry.score > 0) {
          score >= scoreEntry.score
        } else {
          score <= scoreEntry.score
        }
      } else {
        true
      }

      val staticIsMatched = if(static > 0) {
        static >= scoreEntry.score
      } else {
        static <= scoreEntry.static
      }

      scoreIsMatched && staticIsMatched
    }
  }

  override def toString = s"$score:$static POINTS"
}
