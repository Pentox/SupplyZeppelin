# Supply Zeppelin
## About
Supply Zeppelin is a bot that will drop crates with points if chat is active. The first one that clicks the following reaction will get the points.
Peoples can also check a per-guild scoreboard. For more help, check ;help.

I do host an instance which you can [add to your guild](https://discordapp.com/oauth2/authorize?client_id=317248850672746496&scope=bot&permissions=355392).
This runs on my private PC however and will not not be online 100%. It currently serves 25 servers (check with `;stats`).

## Programming
I for myself tried out actors with this bot. It may not be idiomatic in some places. Also, it currently uses an embedded database (hsqldb) with one
SCOREBOARD table (3 columns: `GUILDID LONG, USERID LONG, SCORE INT`) and a rudimentary custom ORM. All of this is subject to change. If you
want to run your own instance, create the table above manually. For running, you must provide your discord api token, your user id (for ;stop etc.), the emote id to use as crate and the JDBC url in that order,
for example `java -jar DiscordEmbed.jar 1234567890123456789 AbCdEfGhIjKlMnOpQ… 1234567890123456789 jdbc:hsqldb:file:/home/example/Supply Zeppelin/db/main`.

Contributions with merge requests and issues are welcome!
