import discord
import asyncio
from discord.ext import commands
import re

class SupplyZeppelin:
  def __init__(self, bot):
    self.authorised = [317248850672746496]
    self.bot = bot
    self.pattern = re.compile("<@!?[0-9]+>(?:, <@!?[0-9]+>)*(?: and all other users above)? has/have each received ([1-9][0-9]*) points from <@!?([0-9]*)>!") 
   
  async def on_message(self, message):
    if(not message.author.bot):
    	return
    
    if(message.mentions.count(message.server.me) == 1):
      if(self.authorised.count(int(message.author.id))):
        match = self.pattern.match(message.content)
        if(match):
          value = int(match.group(1))
          member = message.server.get_member(match.group(2))
          if(member):
            transact = value * 6
            bank = self.bot.get_cog('Economy').bank
            if(not bank.account_exists(member)):
              bank.create_account(member)
            bank.deposit_credits(member, transact)
            await self.bot.send_message(message.channel, "You have successfully exchanged " + str(value) + " points into " + str(transact) + " credits!")
def setup(bot):
  bot.add_cog(SupplyZeppelin(bot))
